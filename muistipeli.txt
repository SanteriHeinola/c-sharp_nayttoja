using System;

using System.Collections.Generic;

using System.Linq;
 
using System.Text;

using System.Threading.Tasks;



namespace oikeaMuistipeli

{

    class Program

    {

        static void Main(string[] args)

        {
            //mahdollistaa lukujen arpomisen
            Random random = new Random();

            Console.WriteLine("5 sekunttia aikaa painaa numerot mieleesi!");
            //alustetaan arvottavat numerot
            int[] numerot = new int[7];

            for (int i = 0; i < numerot.Length; i++)
                
            {
                numerot[i] = random.Next(1, 6);
                //arpoo luodut numerot 1-7 väliltä 1-6
                Console.Write(numerot[i] + " ");
                       //tulostaa numeroiden väliin... välin

            }

            //näyttää numerot 5 sekunttia
            System.Threading.Thread.Sleep(5000);
            Console.Clear();

            //alustetaan arvaus luvut
            int[] nro = new int[7];
            Console.Write("Syötä 7 numeroa väliltä 1-5");
            for (int i = 0; i < nro.Length; i++)
                
            {
                Console.Write("\nAnna numero "+ i + ": ");
                //annetaan arvattaville numeroille arvo
                nro[i] = Convert.ToInt32(Console.ReadLine());
                //muistaa syötteen myöhemmälle
            }

            //printataan numerosi
            Console.Write("\nSyötetyt numerot: ");
            for (int i = 0; i < nro.Length; i++)
                
            {
                Console.Write(nro[i] + " ");
            }

            Console.Write("\nOikeat numerot: ");
            for (int i = 0; i < numerot.Length; i++)

            {
                Console.Write(numerot[i] + " ");
            }

            int[] bucket = new int[256];
            foreach (var b in nro)
            {
                bucket[b]++;
            }

            foreach (var b in numerot)
            {
                bucket[b]--;
            }

            double percent = 100;
            for (int i = 0; i < 256; ++i)

            {
                double val = 100 * ((double)bucket[i] / numerot.Length);
                percent -= Math.Abs(val);
            }

            Console.WriteLine(string.Format("\nNumeroista {0} % on oiken ", percent));

            Console.ReadKey();

        }

    }

}